package com.example.dispatcher.service;

import com.example.dispatcher.model.MovementReport;
import com.example.dispatcher.repository.MovementReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovementReportService {
    @Autowired
    private MovementReportRepository repository;

    public void saveMovementReports(List<MovementReport> movementReports) {
        repository.saveAll(movementReports);
    }
}
