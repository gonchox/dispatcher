package com.example.dispatcher.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "ESBEvent")
@Data
public class MovementReport {
    @Id
    private String _id;
    private String subscriptionId;
    private String frameworkAgreementId;
    private String paymentMethodType;
    private List<Product> products;
    private String traceId;
    private String alias;
    private String emailAddress;
    private String transactionDateTime;
    private String creationDate;
    private String subscriptionStatus;
    private boolean activation;
}
