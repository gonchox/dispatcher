package com.example.dispatcher.repository;

import com.example.dispatcher.model.MovementReport;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovementReportRepository extends MongoRepository<MovementReport, String> {
}
