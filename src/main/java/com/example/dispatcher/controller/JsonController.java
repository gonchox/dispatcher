package com.example.dispatcher.controller;

import com.example.dispatcher.model.MovementReport;
import com.example.dispatcher.repository.MovementReportRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Sinks;

import java.io.IOException;
import java.util.List;

@RestController
public class JsonController {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonController.class);
    private final Sinks.Many<Message<String>> many;
    private final MovementReportRepository movementReportRepository;


    public JsonController(Sinks.Many<Message<String>> many, MovementReportRepository movementReportRepository) {
        this.many = many;
        this.movementReportRepository = movementReportRepository;
    }

    @PostMapping("/json")
    public void handleJsonFile(@RequestBody String json) throws IOException {

       many.emitNext(new GenericMessage<>(json), Sinks.EmitFailureHandler.FAIL_FAST);
       // LOGGER.info("New message emitted and saved to MongoDB: '{}'", json);
    }
}
