package com.example.dispatcher;

import com.example.dispatcher.model.MovementReport;
import com.example.dispatcher.repository.MovementReportRepository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
public class DispatcherApplication implements CommandLineRunner {
	private static final Logger LOGGER = LoggerFactory.getLogger(DispatcherApplication.class);
	private final MovementReportRepository movementReportRepository;
	private static final Sinks.Many<Message<String>> many = Sinks.many().unicast().onBackpressureBuffer();

	public DispatcherApplication(MovementReportRepository movementReportRepository) {
		this.movementReportRepository = movementReportRepository;
	}


	public static void main(String[] args) {
		SpringApplication.run(DispatcherApplication.class, args);
	}

	@Bean
	public Supplier<Flux<Message<String>>> supply() {
		return ()->many.asFlux()
				.doOnNext(m->LOGGER.info("Manually sending message {}", m))
				.doOnError(t->LOGGER.error("Error encountered", t))
				.doFirst(() -> many.asFlux().subscribe(consume()))
				.onBackpressureBuffer()
				.publish()
				.autoConnect();
	}

	@Bean
	public Consumer<Message<String>> consume() {
		return message -> {
			ObjectMapper objectMapper = new ObjectMapper();
			List<MovementReport> movementReports;
			try {
				movementReports = objectMapper.readValue(message.getPayload(), new TypeReference<List<MovementReport>>(){});
				for (MovementReport report : movementReports) {
					movementReportRepository.save(report);
				}
				//many.emitNext(message, Sinks.EmitFailureHandler.FAIL_FAST);
				LOGGER.info("Nuevo mensaje recibido y guardado en la base de datos: '{}'", message.getPayload());
			} catch (IOException e) {
				LOGGER.error("Error al guardar el mensaje en la base de datos", e);
			}
		};
	}

	@Override
	public void run(String... args) {
		//supply().get().subscribe();
	}

	@Bean
	public Sinks.Many<Message<String>> many() {
		return many;
	}

}
